#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include<sys/wait.h>


int main(){
    pid_t pid = fork();
    if(pid == 0){
        int ret = execl("./annexeB","./annexeB",NULL);
        if (ret == -1) {
            perror("execl");
        }
    } else if (pid < 0) {
        perror("fork");
    } else {
        int waitresult;
        waitpid(pid,&waitresult,0);
        printf("sortie de l'annexe B :%i // %i\n", getpid() ,waitresult);
    }
}