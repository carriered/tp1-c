#include <sys/types.h>
#include <unistd.h>
#include <stdint.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include<sys/wait.h>
#include <string.h>

#define MAX_COMMANDE 64

int main(void) {
    int waitResult;

    while (1) {
        //création de l'invite et entrée utilisateur
        printf(">> ");
        char *commandString = malloc(512 * sizeof(char));

        fgets(commandString, 512 * sizeof(char), stdin);

        //séparation des attributs dans un tableau
        char** commande = (char **)malloc(MAX_COMMANDE * sizeof(char*));

        for (int i = 0; i < MAX_COMMANDE; i++) {
            commande[i] = (char*)malloc(64 * sizeof(char));

            if(*(commande+i) == NULL) {
                printf("erreur d'allocation mémoire\n");
                exit(1);
            }
        }

        int i = 0;
        int j = 0; //nb mots
        int l;
        char *buffer = malloc(64 * sizeof(char));
        char c = ' ';
        while (c != '\n'){
            c = *(commandString+i);
            i++;

            if(c == ' ' || c == '\n'){
                strcpy(commande[j],buffer);
                buffer[0] = '\0';
                j++;
            } else{
                l = strlen(buffer);
                buffer[l] = c;
                buffer[l+1] = '\0';
            }
        }
        int background;

        if(strcmp(commande[j-1],"&") == 0){
            background = 1;
        } else{
            background = 0;
        }
        commande[j] = NULL; //pour le execvp dernier element null


        //création du nouveau processus pour effectuer la commande
        pid_t pid = fork();
        if (pid == 0) { //fils
            //execute les commandes en entrée
            execvp(commande[0], &commande[0]);
            perror("execvp");
            exit(1);
        }
        else if (pid < 0) { //erreur
            perror("fork");
        }

        //père
        if(!background){
            wait(&waitResult);
            if (waitResult != 0) {
                printf("end status : %d\n",waitResult);
            }
        }

        //liberer la mémoire
        free(buffer);
        for (int k = 0; k < j; ++k) {
            free(commande[k]);
        }
        free(commande);
        free(commandString);
    }
}